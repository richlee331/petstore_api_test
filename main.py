import unittest
import requests

def post_pet(id, name, photoUrls, status):

    url = "https://petstore.swagger.io/v2/pet"
    headers = {'accept': 'application/json', 'Content-Type': 'application/json'}

    category = {}       #collection with id and name
    photoUrlList = []   #list of strings that are urls
    tags = []           #list of collections with id and name

    category = {"id": id, "name": name}
    if isinstance(photoUrls, list):
        photoUrlList = photoUrls
    else:
        photoUrlList = [photoUrls]
    tags = [{"id": id, "name": name}]

    my_data = { "id": id, "category": category, "name": name, "photoUrls": photoUrlList,
                "tags": tags, "status": status}

    #print("data is:%s" %my_data)
    #print("headers:%s" %headers)

    response = requests.post(url, headers=headers, json=my_data)

    #print(response.status_code)
    print(response.json())
    return response

class TestPetstoreApi(unittest.TestCase):

    def test_post_pet(self):
        test_id = 1
        test_name = "benny"
        test_photoUrls = "http://photos.example.com/benny.jpg"
        test_status = "available"
        res = post_pet(test_id, test_name, test_photoUrls, test_status)
        res_json = res.json()

        self.assertEqual(res.status_code, 200, "pet post api not returning 200 status code")
        self.assertEqual(res_json['name'], test_name, "name did not show %s" %test_name)
        self.assertEqual(res_json['category']['name'], test_name, "category name did not show %s" %test_name)
        self.assertEqual(res_json['id'], res_json['category']['id'], "id and category id did not match")


    def test_post_pet_emoji_name(self):
        test_id = 1
        test_name = "benny❤"
        test_photoUrls = "http://photos.example.com/benny.jpg"
        test_status = "available"
        res = post_pet(test_id, test_name, test_photoUrls, test_status)
        res_json = res.json()

        self.assertEqual(res.status_code, 200, "pet post api not returning 200 status code")
        self.assertEqual(res_json['name'], test_name, "name did not show %s" %test_name)
        self.assertEqual(res_json['category']['name'], test_name, "category name did not show %s" %test_name)

if __name__ == '__main__':
    unittest.main(verbosity=2)
